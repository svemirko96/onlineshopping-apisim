package student;

import operations.BuyerOperations;
import operations.GeneralOperations;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class pa150337_GeneralOperations implements GeneralOperations {

    @Override
    public void setInitialTime(Calendar time) {

        Connection con = DB.getInstance().getConnection();

        String sqlDelete = "DELETE FROM Simulation";

        try(Statement stmt = con.createStatement()) {

            stmt.executeUpdate(sqlDelete);

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String sqlInsert = "INSERT INTO Simulation(SimulationTime) VALUES (?)";

        try(PreparedStatement stmt = con.prepareStatement(sqlInsert)) {

            stmt.setTimestamp(1, new Timestamp(time.getTimeInMillis()));

            stmt.executeUpdate();

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    @Override
    public Calendar time(int days) {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT SimulationTime FROM Simulation";

        Calendar time = null;

        try(Statement stmt = con.createStatement()) {

            ResultSet rs = stmt.executeQuery(sqlSelect);
            if(rs.next()) {
                time = Calendar.getInstance();
                time.setTimeInMillis(rs.getTimestamp("SimulationTime").getTime());
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        time.add(Calendar.DAY_OF_MONTH, days);

        String sqlUpdate = "UPDATE Simulation SET SimulationTime = ?";

        try(PreparedStatement stmt = con.prepareStatement(sqlUpdate)) {

            stmt.setTimestamp(1, new Timestamp(time.getTimeInMillis()));

            int affectedRows = stmt.executeUpdate();
            if(affectedRows == 0) {
                return null;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String getSentOrdersSql = "SELECT o.OrderId, o.SentTime, b.CityId FROM [Order] o, Buyer b WHERE o.BuyerId = b.BuyerId AND [State] = 'sent'";

        List<Integer> sentOrderIds = new ArrayList<>();
        List<Calendar> sentOrderSentTimes = new ArrayList<>();
        List<Integer> buyerCityIds = new ArrayList<>();
        try(Statement stmt = con.createStatement()) {

            ResultSet rs = stmt.executeQuery(getSentOrdersSql);
            while(rs.next()) {
                sentOrderIds.add(rs.getInt("OrderId"));

                Calendar sentTime = Calendar.getInstance();
                sentTime.setTimeInMillis(rs.getTimestamp("SentTime").getTime());
                sentOrderSentTimes.add(sentTime);

                buyerCityIds.add(rs.getInt("CityId"));
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        BuyerOperations buyerOperations = new pa150337_BuyerOperations();
        for(int i = 0; i < sentOrderIds.size(); i++) {

            String getOrderCitiesSql = "SELECT DISTINCT sh.CityId FROM OrderItem oi, Article ar, Shop sh WHERE oi.OrderId = ? AND oi.ArticleId = ar.ArticleId AND ar.ShopId = sh.ShopId";

            List<Integer> orderCityIds = new ArrayList<>();
            try(PreparedStatement stmt = con.prepareStatement(getOrderCitiesSql)) {

                stmt.setInt(1, sentOrderIds.get(i));

                ResultSet rs = stmt.executeQuery();
                while(rs.next()) {
                    orderCityIds.add(rs.getInt("CityId"));
                }

            } catch (SQLException e) {
                Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
            }

            int maxDist = 0;
            for(Integer cityId : orderCityIds) {
                int dist = CityDistanceUtil.minimalDistance(buyerCityIds.get(i), cityId);
                if(dist > maxDist)
                    maxDist = dist;
            }

            Calendar receiveTime = sentOrderSentTimes.get(i);
            receiveTime.add(Calendar.DAY_OF_MONTH, maxDist);

            if(time.compareTo(receiveTime) >= 0) {
                String setOrderArrivedSql = "UPDATE [Order] SET [State] = 'arrived', ReceivedTime = ? WHERE OrderId = ?";

                try(PreparedStatement stmt = con.prepareStatement(setOrderArrivedSql)) {

                    stmt.setTimestamp(1, new Timestamp(receiveTime.getTimeInMillis()));
                    stmt.setInt(2, sentOrderIds.get(i));

                    int affectedRows = stmt.executeUpdate();
                    if(affectedRows == 0) {
                        return null;
                    }

                } catch (SQLException e) {
                    Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
                }

            }

        }

        return time;
    }

    @Override
    public Calendar getCurrentTime() {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT SimulationTime FROM Simulation";

        Calendar time = null;

        try(Statement stmt = con.createStatement()) {

            ResultSet rs = stmt.executeQuery(sqlSelect);
            if(rs.next()) {
                time = Calendar.getInstance();
                time.setTimeInMillis(rs.getTimestamp("SimulationTime").getTime());
                return time;
            }

            return null;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public void eraseAll() {

        Connection con = DB.getInstance().getConnection();

        String sql = "EXEC SPEraseAllData";

        try (CallableStatement cs = con.prepareCall(sql)) {

            cs.execute();

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

    }

}
