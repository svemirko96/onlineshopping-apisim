package student;

import operations.ShopOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class pa150337_ShopOperations implements ShopOperations {

    @Override
    public int createShop(String name, String cityName) {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT CityId FROM City WHERE Name = ?";

        int cityId = -1;

        try(PreparedStatement stmt = con.prepareStatement(sqlSelect)) {

            stmt.setString(1, cityName);
            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                cityId = rs.getInt("CityId");
            }
            else {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String sqlInsert = "INSERT INTO Shop(Name, Discount, CityId) VALUES (?, 0, ?)";

        try(PreparedStatement stmt = con.prepareStatement(sqlInsert, Statement.RETURN_GENERATED_KEYS)) {

            stmt.setString(1, name);
            stmt.setInt(2, cityId);

            int affectedRows = stmt.executeUpdate();
            if(affectedRows == 0) {
                return -1;
            }

            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if(rs.next()) {
                    return (int)rs.getLong(1);
                }
                else {
                    return -1;
                }
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public int setCity(int shopId, String cityName) {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT CityId FROM City WHERE Name = ?";

        int cityId = -1;

        try(PreparedStatement stmt = con.prepareStatement(sqlSelect)) {

            stmt.setString(1, cityName);
            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                cityId = rs.getInt("CityId");
            }
            else {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String sqlUpdate = "UPDATE Shop SET CityId = ? WHERE ShopId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sqlUpdate)) {

            stmt.setInt(1, cityId);
            stmt.setInt(2, shopId);

            int rowsAffected = stmt.executeUpdate();

            if(rowsAffected == 0) {
                return -1;
            }

            return 1;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public int getCity(int shopId) {

        Connection con = DB.getInstance().getConnection();

        String sql = "SELECT CityId FROM Shop WHERE ShopId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sql)) {

            stmt.setInt(1, shopId);
            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                return rs.getInt("CityId");
            }
            else {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public int setDiscount(int shopId, int discountPercentage) {

        Connection con = DB.getInstance().getConnection();

        String sql = "UPDATE Shop SET Discount = ? WHERE ShopId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sql)) {

            stmt.setInt(1, discountPercentage);
            stmt.setInt(2, shopId);
            int affectedRows = stmt.executeUpdate();

            if(affectedRows == 0) {
                return -1;
            }

            return 1;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public int increaseArticleCount(int articleId, int increment) {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT Count FROM Article WHERE ArticleId = ?";

        int count = 0;

        try(PreparedStatement stmt = con.prepareStatement(sqlSelect)) {

            stmt.setInt(1, articleId);
            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                count = rs.getInt("Count");
            }
            else {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String sqlInsert = "UPDATE Article SET Count = ? WHERE ArticleId = ?";

        count += increment;

        try(PreparedStatement stmt = con.prepareStatement(sqlInsert)) {

            stmt.setInt(1, count);
            stmt.setInt(2, articleId);

            int affectedRows = stmt.executeUpdate();
            if(affectedRows == 0) {
                return -1;
            }

            return count;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public int getArticleCount(int articleId) {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT Count FROM Article WHERE ArticleId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sqlSelect)) {

            stmt.setInt(1, articleId);
            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                return rs.getInt("Count");
            }
            else {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public List<Integer> getArticles(int shopId) {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT ArticleId FROM Article WHERE ShopId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sqlSelect)) {

            stmt.setInt(1, shopId);
            ResultSet rs = stmt.executeQuery();

            List<Integer> articleIds = new ArrayList<>();

            while(rs.next()) {
                 articleIds.add(rs.getInt("ArticleId"));
            }

            return articleIds;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public int getDiscount(int shopId) {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT Discount FROM Shop WHERE ShopId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sqlSelect)) {

            stmt.setInt(1, shopId);
            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                return rs.getInt("Discount");
            }
            else {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

}
