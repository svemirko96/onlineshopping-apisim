package student;

import operations.CityOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class pa150337_CityOperations implements CityOperations {

    @Override
    public int createCity(String name) {

        Connection con = DB.getInstance().getConnection();

        String sql = "INSERT INTO City(Name) VALUES (?)";

        try (PreparedStatement stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            stmt.setString(1, name);

            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                return -1;
            }

            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if(rs.next()) {
                    return (int)rs.getLong(1);
                }
                else {
                    return -1;
                }
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public List<Integer> getCities() {

        Connection con = DB.getInstance().getConnection();

        String sql = "SELECT CityId FROM City";

        try (Statement stmt = con.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);

            List<Integer> cityIds = new ArrayList<>();
            while(rs.next()) {
                cityIds.add(rs.getInt("CityId"));
            }

            return cityIds;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public int connectCities(int cityId1, int cityId2, int distance) {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT ConnectionId FROM CityConnection WHERE (City1Id = ? AND City2Id = ?) OR (City1Id = ? AND City2Id = ?)";

        try (PreparedStatement ps = con.prepareStatement(sqlSelect)) {

            ps.setInt(1, cityId1);
            ps.setInt(2, cityId2);
            ps.setInt(3, cityId2);
            ps.setInt(4, cityId1);
            ResultSet rs = ps.executeQuery();

            if(rs.next()) {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String sqlInsert = "INSERT INTO CityConnection(City1Id, City2Id, Distance) VALUES (?, ?, ?)";

        try (PreparedStatement ps = con.prepareStatement(sqlInsert, Statement.RETURN_GENERATED_KEYS)) {

            ps.setInt(1, cityId1);
            ps.setInt(2, cityId2);
            ps.setInt(3, distance);

            int rowsAffected = ps.executeUpdate();
            if(rowsAffected == 0) {
                return -1;
            }

            try (ResultSet rs = ps.getGeneratedKeys()) {
                if(rs.next()) {
                    return (int)rs.getLong(1);
                }
                else {
                    return -1;
                }
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public List<Integer> getConnectedCities(int cityId) {

        Connection con = DB.getInstance().getConnection();

        String sql = "SELECT City1Id, City2Id FROM CityConnection WHERE City1Id = ? OR City2Id = ?";

        try (PreparedStatement stmt = con.prepareStatement(sql)) {

            stmt.setInt(1, cityId);
            stmt.setInt(2, cityId);
            ResultSet rs = stmt.executeQuery();

            List<Integer> connectedCities = new ArrayList<>();
            while(rs.next()) {
                int city1Id = rs.getInt("City1Id");
                int city2Id = rs.getInt("City2Id");
                if(cityId == city1Id) {
                    connectedCities.add(city2Id);
                }
                else {
                    connectedCities.add(city1Id);
                }
            }

            return connectedCities;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public List<Integer> getShops(int cityId) {

        Connection con = DB.getInstance().getConnection();

        String sql = "SELECT ShopId FROM Shop WHERE CityId = ?";

        try (PreparedStatement stmt = con.prepareStatement(sql)) {

            stmt.setInt(1, cityId);
            ResultSet rs = stmt.executeQuery();

            List<Integer> shopIds = new ArrayList<>();
            while(rs.next()) {
                shopIds.add(rs.getInt("ShopId"));
            }

            return shopIds;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

}
