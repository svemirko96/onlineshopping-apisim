package student;

import operations.CityOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CityDistanceUtil {

    public static int minimalDistance(int source, int destination) {
        return dijkstra(source, destination).get(0);
    }

    public static List<Integer> getTheShortestPathBetween(int source, int destination) {
        List<Integer> result = dijkstra(source, destination);
        result.remove(0);
        return result;
    }

    private static List<Integer> dijkstra(int source, int destination) {

        CityOperations cityOperations = new pa150337_CityOperations();
        List<Integer> cityIds = cityOperations.getCities();

        final int NUM_CITIES = cityIds.size();
        int[][] distances = new int[NUM_CITIES][NUM_CITIES];
        for(int i = 0; i < NUM_CITIES; i++)
            for(int j = 0; j < NUM_CITIES; j++)
                distances[i][j] = -1;

        Map<Integer, Integer> cityIdToGraphNode = new HashMap<>();
        for(int i = 0; i < NUM_CITIES; i++)
            cityIdToGraphNode.put(cityIds.get(i), i);

        Connection con = DB.getInstance().getConnection();

        String getDistancesSql = "SELECT * FROM CityConnection";

        try(Statement stmt = con.createStatement()) {

            ResultSet rs = stmt.executeQuery(getDistancesSql);
            while(rs.next()) {
                int city1Index = cityIdToGraphNode.get(rs.getInt("City1Id"));
                int city2Index = cityIdToGraphNode.get(rs.getInt("City2Id"));
                int distance = rs.getInt("Distance");
                distances[city1Index][city2Index] = distances[city2Index][city1Index] = distance;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        // Dijkstra
        // Set all distances from source to INF (-1) and to itself 0 in array dist
        // Add all cities to the list of unprocessedCities
        int[] dist = new int[NUM_CITIES];
        int[] prev = new int[NUM_CITIES];
        List<Integer> unprocessedCities = new ArrayList<>();
        for(int i = 0; i < NUM_CITIES; i++) {
            dist[i] = -1;
            prev[i] = -1;
            unprocessedCities.add(cityIds.get(i));
        }
        dist[cityIdToGraphNode.get(source)] = 0;

        while(!unprocessedCities.isEmpty()) {
            // Find the next nearest unprocessed city using dist array and remove it from the list of unprocessed
            int minDist = Integer.MAX_VALUE;
            int nearestCityId = -1;
            int toRemoveIndex = -1;
            for(int i = 0; i < unprocessedCities.size(); i++) {
                int otherId = unprocessedCities.get(i);
                int otherIndex = cityIdToGraphNode.get(otherId);
                int d = dist[otherIndex];
                if(d >= 0 && d  < minDist) {
                    minDist = d;
                    nearestCityId = otherId;
                    toRemoveIndex = i;
                }
            }
            unprocessedCities.remove(toRemoveIndex);

            int nearestIndex = cityIdToGraphNode.get(nearestCityId);
            for(int i = 0; i < NUM_CITIES; i++) {
                int d = distances[nearestIndex][i];
                if(d >= 0) {
                    int alt = dist[nearestIndex] + distances[nearestIndex][i];
                    if(dist[i] < 0 || alt < dist[i]) {
                        dist[i] = alt;
                        prev[i] = nearestIndex;
                    }
                }
            }

        }

//        for(int i = 0; i < NUM_CITIES; i++) {
//            for(int j = 0; j < NUM_CITIES; j++)
//                System.out.print(distances[i][j] + "\t");
//            System.out.println();
//        }

//        System.out.println("Distances from city " + source + " to all other cities: ");
//        for(int d : dist) System.out.print(d + " ");
//        System.out.println();

//        for(int p : prev) System.out.print(p + " ");
//        System.out.println();

        // Result of the method is a list which contains minimal distance to the destination node as the first element,
        // and the rest are list of city ids on the shortest path from source to destination including source and destination
        List<Integer> result = new ArrayList<>();
        result.add(0, destination);
        int prevIndex = prev[cityIdToGraphNode.get(destination)];
        while(prevIndex >= 0) {
            result.add(0, cityIds.get(prevIndex));
            prevIndex = prev[prevIndex];
        }
        result.add(0, dist[cityIdToGraphNode.get(destination)]);

        return result;
    }

}
