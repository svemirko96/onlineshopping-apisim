package student;

import operations.ArticleOperations;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class pa150337_ArticleOperations implements ArticleOperations {

    @Override
    public int createArticle(int shopId, String articleName, int articlePrice) {

        Connection con = DB.getInstance().getConnection();

        String sql = "INSERT INTO Article(Name, Price, Count, ShopId) VALUES (?, ?, 0, ?)";

        try (PreparedStatement stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            stmt.setString(1, articleName);
            stmt.setInt(2, articlePrice);
            stmt.setInt(3, shopId);

            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                return -1;
            }

            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if(rs.next()) {
                    return (int)rs.getLong(1);
                }
                else {
                    return -1;
                }
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

}
