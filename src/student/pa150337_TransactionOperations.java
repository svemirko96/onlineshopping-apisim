package student;

import operations.TransactionOperations;

import javax.swing.plaf.nimbus.State;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class pa150337_TransactionOperations implements TransactionOperations {

    @Override
    public BigDecimal getBuyerTransactionsAmmount(int buyerId) {

        Connection con = DB.getInstance().getConnection();

        String getAmountSql = "SELECT SUM(Value) AS Amount FROM [Transaction] tr, [Order] [or] WHERE tr.OrderId = [or].OrderId AND [or].BuyerId = ?";

        try (PreparedStatement stmt = con.prepareStatement(getAmountSql)) {

            stmt.setInt(1, buyerId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                BigDecimal amount = rs.getBigDecimal("Amount");
                if(amount == null)
                    return new BigDecimal("0.000").setScale(3);
                return amount;
            }
            else {
                return new BigDecimal("0.000").setScale(3);
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public BigDecimal getShopTransactionsAmmount(int shopId) {

        Connection con = DB.getInstance().getConnection();

        String getAmountSql = "SELECT SUM(Value) AS Amount FROM [Transaction] tr WHERE tr.ShopId = ?";

        try (PreparedStatement stmt = con.prepareStatement(getAmountSql)) {

            stmt.setInt(1, shopId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                BigDecimal amount = rs.getBigDecimal("Amount");
                if(amount == null)
                    return new BigDecimal("0").setScale(3);
                return amount;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public List<Integer> getTransationsForBuyer(int buyerId) {

        Connection con = DB.getInstance().getConnection();

        String getTransactionsSql = "SELECT tr.TransactionId FROM [Transaction] tr, [Order] [or] WHERE tr.OrderId = [or].OrderId AND BuyerId = ?";

        List<Integer> transactionIds = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement(getTransactionsSql)) {

            stmt.setInt(1, buyerId);

            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                transactionIds.add(rs.getInt("TransactionId"));
            }

            return transactionIds;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public int getTransactionForBuyersOrder(int orderId) {

        Connection con = DB.getInstance().getConnection();

        String getTransactionSql = "SELECT TransactionId FROM [Transaction] WHERE OrderId = ?";

        try (PreparedStatement stmt = con.prepareStatement(getTransactionSql)) {

            stmt.setInt(1, orderId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs.getInt("TransactionId");
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public int getTransactionForShopAndOrder(int orderId, int shopId) {

        Connection con = DB.getInstance().getConnection();

        String getTransactionSql = "SELECT TransactionId FROM [Transaction] WHERE OrderId = ? AND ShopId = ?";

        try (PreparedStatement stmt = con.prepareStatement(getTransactionSql)) {

            stmt.setInt(1, orderId);
            stmt.setInt(2, shopId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs.getInt("TransactionId");
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public List<Integer> getTransationsForShop(int shopId) {

        Connection con = DB.getInstance().getConnection();

        String getTransactionsSql = "SELECT TransactionId FROM [Transaction] WHERE ShopId = ?";

        List<Integer> transactionIds = new ArrayList<>();
        try (PreparedStatement stmt = con.prepareStatement(getTransactionsSql)) {

            stmt.setInt(1, shopId);

            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                transactionIds.add(rs.getInt("TransactionId"));
            }

            if(transactionIds.size() > 0)
                return transactionIds;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public Calendar getTimeOfExecution(int transactionId) {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT ExecutionTime FROM [Transaction] WHERE TransactionId = ?";

        Calendar time = null;

        try(PreparedStatement stmt = con.prepareStatement(sqlSelect)) {

            stmt.setInt(1, transactionId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                time = Calendar.getInstance();
                time.setTimeInMillis(rs.getTimestamp("ExecutionTime").getTime());
                return time;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public BigDecimal getAmmountThatBuyerPayedForOrder(int orderId) {

        Connection con = DB.getInstance().getConnection();

        String getAmountSql = "SELECT Value FROM [Transaction] WHERE OrderId = ?";

        try(PreparedStatement stmt = con.prepareStatement(getAmountSql)) {

            stmt.setInt(1, orderId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs.getBigDecimal("Value");
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public BigDecimal getAmmountThatShopRecievedForOrder(int shopId, int orderId) {

        Connection con = DB.getInstance().getConnection();

        String getAmountSql = "SELECT Value FROM [Transaction] WHERE OrderId = ? AND ShopId = ?";

        try(PreparedStatement stmt = con.prepareStatement(getAmountSql)) {

            stmt.setInt(1, orderId);
            stmt.setInt(2, shopId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                BigDecimal amount = rs.getBigDecimal("Value");
                if(amount == null)
                    return new BigDecimal("0.000");
            }

            return null;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public BigDecimal getTransactionAmount(int transactionId) {

        Connection con = DB.getInstance().getConnection();

        String getAmountSql = "SELECT Value FROM [Transaction] WHERE TransactionId = ?";

        try(PreparedStatement stmt = con.prepareStatement(getAmountSql)) {

            stmt.setInt(1, transactionId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                BigDecimal amount = rs.getBigDecimal("Value");
                if(amount == null)
                    return new BigDecimal("0").setScale(3);
                return amount;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public BigDecimal getSystemProfit() {

        Connection con = DB.getInstance().getConnection();

        String getSystemProfitSql = "SELECT SUM(trs.[Value] / 0.95 * (0.05 - 0.02 * trb.ExtraDiscount)) AS Amount FROM [Transaction] trs, [Transaction] trb WHERE trs.OrderId = trb.OrderId AND trb.ShopId IS NULL AND trs.ShopId IS NOT NULL";

        try(Statement stmt = con.createStatement()) {

            ResultSet rs = stmt.executeQuery(getSystemProfitSql);
            if(rs.next()) {
                BigDecimal amount = rs.getBigDecimal("Amount");
                if(amount == null)
                    return new BigDecimal("0").setScale(3);
                return amount.setScale(3);
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

}
