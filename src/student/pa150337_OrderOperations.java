package student;

import operations.BuyerOperations;
import operations.GeneralOperations;
import operations.OrderOperations;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class pa150337_OrderOperations implements OrderOperations {

    @Override
    public int addArticle(int orderId, int articleId, int count) {

        Connection con = DB.getInstance().getConnection();

        String getCountInShopSql = "SELECT [Count] FROM Article WHERE ArticleId = ?";
        int countInShop = 0;
        try (PreparedStatement stmt = con.prepareStatement(getCountInShopSql)) {

            stmt.setInt(1, articleId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                countInShop = rs.getInt("Count");
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        if(countInShop < count)
            return -1;

        String updateInShopCountSql = "UPDATE Article SET [Count] = ? WHERE ArticleId = ?";

        try (PreparedStatement stmt = con.prepareStatement(updateInShopCountSql)) {

            int newCount = countInShop - count;
            stmt.setInt(1, newCount);
            stmt.setInt(2, articleId);

            int affectedRows = stmt.executeUpdate();
            if(affectedRows == 0) {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String sqlSelect = "SELECT ItemId FROM OrderItem WHERE OrderId = ? AND ArticleId = ?";
        int itemId = -1;
        try (PreparedStatement stmt = con.prepareStatement(sqlSelect)) {

            stmt.setInt(1, orderId);
            stmt.setInt(2, articleId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                itemId = rs.getInt("ItemId");
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        if(itemId > -1) { // increase the count of the existing one

            String sqlUpdate = "UPDATE OrderItem SET Count = Count + ? WHERE OrderId = ? AND ArticleId = ?";

            try (PreparedStatement stmt = con.prepareStatement(sqlUpdate)) {

                stmt.setInt(1, count);
                stmt.setInt(2, orderId);
                stmt.setInt(3, articleId);

                int affectedRows = stmt.executeUpdate();
                if (affectedRows == 0) {
                    return -1;
                }

                return itemId;

            } catch (SQLException e) {
                Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        else { // insert

            String sqlInsert = "INSERT INTO OrderItem(Count, OrderId, ArticleId) VALUES (?, ?, ?)";

            try (PreparedStatement stmt = con.prepareStatement(sqlInsert, Statement.RETURN_GENERATED_KEYS)) {

                stmt.setInt(1, count);
                stmt.setInt(2, orderId);
                stmt.setInt(3, articleId);

                int affectedRows = stmt.executeUpdate();
                if (affectedRows == 0) {
                    return -1;
                }

                try (ResultSet rs = stmt.getGeneratedKeys()) {
                    if(rs.next()) {
                        return (int)rs.getLong(1);
                    }
                    else {
                        return -1;
                    }
                }

            } catch (SQLException e) {
                Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
            }
        }

        return -1;
    }

    @Override
    public int removeArticle(int orderId, int articleId) {

        Connection con = DB.getInstance().getConnection();

        String updateInShopCount = "UPDATE Article SET [Count] = ar.[Count] + oi.[Count] FROM Article ar, OrderItem oi WHERE oi.OrderId = ? AND oi.ArticleId = ar.ArticleId AND ar.ArticleId = ?";

        try(PreparedStatement stmt = con.prepareStatement(updateInShopCount)) {

            stmt.setInt(1, orderId);
            stmt.setInt(2, articleId);

            int rowsAffected = stmt.executeUpdate();
            if(rowsAffected == 0) {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String deleteOrderItemSql = "DELETE FROM OrderItem WHERE OrderId = ? AND articleId = ?";

        try(PreparedStatement stmt = con.prepareStatement(deleteOrderItemSql)) {

            stmt.setInt(1, orderId);
            stmt.setInt(2, articleId);

            int rowsAffected = stmt.executeUpdate();
            if(rowsAffected == 0) {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return 1;
    }

    @Override
    public List<Integer> getItems(int orderId) {

        Connection con = DB.getInstance().getConnection();

        String sql = "SELECT ItemId FROM OrderItem WHERE OrderId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sql)) {

            stmt.setInt(1, orderId);

            ResultSet rs = stmt.executeQuery();

            List<Integer> itemIds = new ArrayList<>();
            while(rs.next()) {
                itemIds.add(rs.getInt("ItemId"));
            }

            return itemIds;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public int completeOrder(int orderId) {

        Connection con = DB.getInstance().getConnection();

        String testOrderStateSql = "SELECT OrderId FROM [Order] WHERE OrderId = ? AND State='created'";

        try(PreparedStatement stmt = con.prepareStatement(testOrderStateSql)) {

            stmt.setInt(1, orderId);

            ResultSet rs = stmt.executeQuery();
            if(!rs.next()) {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String updateOrderItemsSql = "UPDATE OrderItem SET Discount = (SELECT Discount FROM Shop s, Article a WHERE oi.ArticleId = a.ArticleId AND a.ShopId = s.ShopId) FROM OrderItem oi WHERE OrderId = ?";

        try(PreparedStatement stmt = con.prepareStatement(updateOrderItemsSql)) {

            stmt.setInt(1, orderId);

            int rowsAffected = stmt.executeUpdate();
            if(rowsAffected == 0) {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        BigDecimal finalPrice = getFinalPrice(orderId);

        int buyerId = -1;
        BigDecimal buyerCredit = null;

        String selectBuyerSql = "SELECT b.BuyerId, b.Credit FROM Buyer b, [Order] o WHERE o.OrderId = ? AND o.BuyerId = b.BuyerId";

        try(PreparedStatement stmt = con.prepareStatement(selectBuyerSql)) {

            stmt.setInt(1, orderId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                buyerId = rs.getInt("BuyerId");
                buyerCredit = rs.getBigDecimal("Credit");
            }
            else {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        if(buyerCredit.compareTo(finalPrice) < 0) {
            return -1;
        }

        buyerCredit = buyerCredit.subtract(finalPrice);

        String updateBuyerCreditSql =  "UPDATE Buyer SET Credit = ? WHERE BuyerId = ?";

        try(PreparedStatement stmt = con.prepareStatement(updateBuyerCreditSql)) {

            stmt.setBigDecimal(1, buyerCredit);
            stmt.setInt(2, buyerId);

            int rowsAffected = stmt.executeUpdate();
            if(rowsAffected == 0) {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String getSpentLastMonth = "SELECT SUM(Value) AS Amount" +
            " FROM [Transaction] tr, [Order] [or]" +
	        " WHERE tr.[OrderId] = [or].OrderId AND" +
			" [or].OrderId != ? AND" +
			" tr.ShopId IS NULL AND" +
			" [or].BuyerId = (SELECT BuyerId FROM [Order] or1 WHERE or1.OrderId = ?) AND" +
			" DATEDIFF(DAY, tr.ExecutionTime, (SELECT SimulationTime FROM Simulation)) <= 30";

        BigDecimal spentLastMonth = null;
        try(PreparedStatement stmt = con.prepareStatement(getSpentLastMonth)) {

            stmt.setInt(1, orderId);
            stmt.setInt(2, orderId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                spentLastMonth = rs.getBigDecimal("Amount");
                if(spentLastMonth == null)
                    spentLastMonth = new BigDecimal("0.000").setScale(3);
            }
            else {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String createTransactionSql = "INSERT INTO [Transaction](Value, ExecutionTime, OrderId, ShopId, ExtraDiscount) VALUES (?, (SELECT SimulationTime FROM Simulation), ?, NULL, ?)";

        try(PreparedStatement stmt = con.prepareStatement(createTransactionSql)) {

            boolean extraDiscount = spentLastMonth.compareTo(new BigDecimal("10000.000").setScale(3)) > 0;

            stmt.setBigDecimal(1, finalPrice);
            stmt.setInt(2, orderId);
            stmt.setBoolean(3, extraDiscount);

            int rowsAffected = stmt.executeUpdate();
            if(rowsAffected == 0) {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String updateSentTimeSql = "UPDATE [Order] SET SentTime = (SELECT SimulationTime FROM Simulation), State='sent' WHERE OrderId = ?";

        try(PreparedStatement stmt = con.prepareStatement(updateSentTimeSql)) {

            stmt.setInt(1, orderId);

            int rowsAffected = stmt.executeUpdate();
            if(rowsAffected == 0) {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return 1;
    }

    @Override
    public BigDecimal getFinalPrice(int orderId) {

        Connection con = DB.getInstance().getConnection();

        String sql = "EXEC dbo.SP_FINAL_PRICE ?, ?";

        try(CallableStatement stmt = con.prepareCall(sql)) {

            stmt.setInt(1, orderId);
            stmt.registerOutParameter(2, Types.DECIMAL);

            stmt.execute();
            return stmt.getBigDecimal(2).setScale(3);

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public BigDecimal getDiscountSum(int orderId) {

        Connection con = DB.getInstance().getConnection();

        String sql = "EXEC dbo.SPDiscountSum ?, ?";

        try(CallableStatement stmt = con.prepareCall(sql)) {

            stmt.setInt(1, orderId);
            stmt.registerOutParameter(2, Types.DECIMAL);

            stmt.execute();
            return stmt.getBigDecimal(2).setScale(3);

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public String getState(int orderId) {

        Connection con = DB.getInstance().getConnection();

        String sql = "SELECT State FROM [Order] WHERE OrderId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sql)) {

            stmt.setInt(1, orderId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs.getString("State");
            }

            return null;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public Calendar getSentTime(int orderId) {

        Connection con = DB.getInstance().getConnection();

        String sql = "SELECT SentTime FROM [Order] WHERE OrderId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sql)) {

            stmt.setInt(1, orderId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                Timestamp timestamp = rs.getTimestamp("SentTime");
                if(timestamp == null)
                    return null;

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(timestamp.getTime());
                return calendar;
            }

            return null;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public Calendar getRecievedTime(int orderId) {

        Connection con = DB.getInstance().getConnection();

        String sql = "SELECT ReceivedTime FROM [Order] WHERE OrderId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sql)) {

            stmt.setInt(1, orderId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                Timestamp timestamp = rs.getTimestamp("ReceivedTime");
                if(timestamp == null)
                    return null;
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(timestamp.getTime());
                return calendar;
            }

            return null;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public int getBuyer(int orderId) {

        Connection con = DB.getInstance().getConnection();

        String sql = "SELECT BuyerId FROM [Order] WHERE OrderId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sql)) {

            stmt.setInt(1, orderId);

            ResultSet rs = stmt.executeQuery();
            if(rs.next()) {
                return rs.getInt("BuyerId");
            }

            return -1;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public int getLocation(int orderId) {

        String orderState = getState(orderId);
        if(orderState.equals("created")) return -1;

        int buyerId = getBuyer(orderId);
        BuyerOperations buyerOperations = new pa150337_BuyerOperations();
        int buyerCityId = buyerOperations.getCity(buyerId);

        if(orderState.equals("arrived")) return buyerCityId;

        Connection con = DB.getInstance().getConnection();

        List<Integer> shopCityIds = new ArrayList<>();

        String getCitiesSql = "SELECT sh.CityId FROM OrderItem oi, Article ar, Shop sh WHERE oi.OrderId = ? AND oi.ArticleId = ar.ArticleId AND ar.ShopId = sh.ShopId";

        try(PreparedStatement stmt = con.prepareStatement(getCitiesSql)) {

            stmt.setInt(1, orderId);

            ResultSet rs = stmt.executeQuery();
            while(rs.next()) {
                shopCityIds.add(rs.getInt("CityId"));
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        int nearestCityId = -1;
        int distToNearestShop = Integer.MAX_VALUE;
        for(Integer cityId : shopCityIds) {
            int distance = CityDistanceUtil.minimalDistance(buyerCityId, cityId);
            if(distance < distToNearestShop) {
                distToNearestShop = distance;
                nearestCityId = cityId;
            }
        }

        int maxAssembleDist = 0;
        for(Integer cityId : shopCityIds) {
            int distance = CityDistanceUtil.minimalDistance(nearestCityId, cityId);
            if(distance > maxAssembleDist) {
                maxAssembleDist = distance;
            }
        }

        GeneralOperations generalOperations = new pa150337_GeneralOperations();
        Calendar currentTime = generalOperations.getCurrentTime();
        currentTime.set(Calendar.HOUR, 0);
        currentTime.set(Calendar.MINUTE, 0);
        currentTime.set(Calendar.SECOND, 0);
        currentTime.set(Calendar.MILLISECOND, 0);

        Calendar sentTime = getSentTime(orderId);
        sentTime.set(Calendar.HOUR, 0);
        sentTime.set(Calendar.MINUTE, 0);
        sentTime.set(Calendar.SECOND, 0);
        sentTime.set(Calendar.MILLISECOND, 0);

        int daysSinceSent = (int)TimeUnit.MILLISECONDS.toDays(Math.abs(sentTime.getTimeInMillis() - currentTime.getTimeInMillis()));
        if(daysSinceSent < maxAssembleDist)
            return nearestCityId;

        if(buyerCityId == nearestCityId)
            return buyerCityId;

        daysSinceSent -= maxAssembleDist;
        List<Integer> citiesFromShopToBuyer = CityDistanceUtil.getTheShortestPathBetween(nearestCityId, buyerCityId);
        for(int i = 1; i < citiesFromShopToBuyer.size(); i++) {
            int daysFromShopToCityI = CityDistanceUtil.minimalDistance(nearestCityId, citiesFromShopToBuyer.get(i));
            if(daysSinceSent < daysFromShopToCityI)
                return citiesFromShopToBuyer.get(i-1);
        }

        return buyerCityId;
    }

}
