package student;

import operations.BuyerOperations;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class pa150337_BuyerOperations implements BuyerOperations {

    @Override
    public int createBuyer(String name, int cityId) {

        Connection con = DB.getInstance().getConnection();

        String sqlInsert = "INSERT INTO Buyer(Name, Credit, CityId) VALUES (?, 0, ?)";

        try (PreparedStatement ps = con.prepareStatement(sqlInsert, Statement.RETURN_GENERATED_KEYS)) {

            ps.setString(1, name);
            ps.setInt(2, cityId);

            int rowsAffected = ps.executeUpdate();
            if(rowsAffected == 0) {
                return -1;
            }

            try (ResultSet rs = ps.getGeneratedKeys()) {
                if(rs.next()) {
                    return (int)rs.getLong(1);
                }
                else {
                    return -1;
                }
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public int setCity(int buyerId, int cityId) {

        Connection con = DB.getInstance().getConnection();

        String sqlUpdate = "UPDATE Buyer SET CityId = ? WHERE BuyerId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sqlUpdate)) {

            stmt.setInt(1, cityId);
            stmt.setInt(2, buyerId);

            int rowsAffected = stmt.executeUpdate();

            if(rowsAffected == 0) {
                return -1;
            }

            return 1;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public int getCity(int buyerId) {

        Connection con = DB.getInstance().getConnection();

        String sql = "SELECT CityId FROM Buyer WHERE BuyerId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sql)) {

            stmt.setInt(1, buyerId);
            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                return rs.getInt("CityId");
            }
            else {
                return -1;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public BigDecimal increaseCredit(int buyerId, BigDecimal credit) {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT Credit FROM Buyer WHERE BuyerId = ?";

        BigDecimal buyerCredit = null;

        try(PreparedStatement stmt = con.prepareStatement(sqlSelect)) {

            stmt.setInt(1, buyerId);
            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                buyerCredit = rs.getBigDecimal("Credit");
            }
            else {
                return null;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        String sqlUpdate = "UPDATE Buyer SET Credit = ? WHERE BuyerId = ?";

        buyerCredit = buyerCredit.add(credit);

        try(PreparedStatement stmt = con.prepareStatement(sqlUpdate)) {

            stmt.setBigDecimal(1, buyerCredit);
            stmt.setInt(2, buyerId);

            int affectedRows = stmt.executeUpdate();
            if(affectedRows == 0) {
                return null;
            }

            return buyerCredit;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public int createOrder(int buyerId) {

        Connection con = DB.getInstance().getConnection();

        String sqlInsert = "INSERT INTO [Order](State, BuyerId) VALUES ('created', ?)";

        try (PreparedStatement stmt = con.prepareStatement(sqlInsert, Statement.RETURN_GENERATED_KEYS)) {

            stmt.setInt(1, buyerId);

            int rowsAffected = stmt.executeUpdate();
            if(rowsAffected == 0) {
                return -1;
            }

            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if(rs.next()) {
                    return (int)rs.getLong(1);
                }
                else {
                    return -1;
                }
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return -1;
    }

    @Override
    public List<Integer> getOrders(int buyerId) {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT OrderId FROM [Order] WHERE BuyerId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sqlSelect)) {

            stmt.setInt(1, buyerId);
            ResultSet rs = stmt.executeQuery();

            List<Integer> articleIds = new ArrayList<>();

            while(rs.next()) {
                articleIds.add(rs.getInt("OrderId"));
            }

            return articleIds;

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    @Override
    public BigDecimal getCredit(int buyerId) {

        Connection con = DB.getInstance().getConnection();

        String sqlSelect = "SELECT Credit FROM Buyer WHERE BuyerId = ?";

        try(PreparedStatement stmt = con.prepareStatement(sqlSelect)) {

            stmt.setInt(1, buyerId);
            ResultSet rs = stmt.executeQuery();

            if(rs.next()) {
                return rs.getBigDecimal("Credit");
            }
            else {
                return null;
            }

        } catch (SQLException e) {
            Logger.getLogger(pa150337_ArticleOperations.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

}
