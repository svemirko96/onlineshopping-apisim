import operations.*;
import org.junit.Test;
import student.*;
import tests.TestHandler;
import tests.TestRunner;

import java.util.Calendar;

public class StudentMain {

    public static void main(String[] args) {

        ArticleOperations articleOperations = new pa150337_ArticleOperations();
        BuyerOperations buyerOperations = new pa150337_BuyerOperations();
        CityOperations cityOperations = new pa150337_CityOperations();
        GeneralOperations generalOperations = new pa150337_GeneralOperations();
        OrderOperations orderOperations = new pa150337_OrderOperations();
        ShopOperations shopOperations = new pa150337_ShopOperations();
        TransactionOperations transactionOperations = new pa150337_TransactionOperations();
//
//        Calendar c = Calendar.getInstance();
//        c.clear();
//        c.set(2010, Calendar.JANUARY, 01);
//
//
//        Calendar c2 = Calendar.getInstance();
//        c2.clear();
//        c2.set(2010, Calendar.JANUARY, 01);
//
//        if(c.equals(c2)) System.out.println("jednako");
//        else System.out.println("nije jednako");

        TestHandler.createInstance(
                articleOperations,
                buyerOperations,
                cityOperations,
                generalOperations,
                orderOperations,
                shopOperations,
                transactionOperations
        );

        TestRunner.runTests();
    }
}
