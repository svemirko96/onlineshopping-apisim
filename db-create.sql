
----------------------- tables ----------------------------------

CREATE TABLE [Article]
( 
	[ArticleId]          integer  IDENTITY ( 1,1 )  NOT NULL ,
	[Name]               varchar(100)  NULL ,
	[Price]              integer  NULL ,
	[Count]              integer  NULL ,
	[ShopId]             integer  NOT NULL 
)
go

ALTER TABLE [Article]
	ADD CONSTRAINT [XPKArticle] PRIMARY KEY  CLUSTERED ([ArticleId] ASC)
go

CREATE TABLE [Buyer]
( 
	[BuyerId]            integer  IDENTITY ( 1,1 )  NOT NULL ,
	[Name]               varchar(100)  NULL ,
	[Credit]             decimal(10,3)  NULL ,
	[CityId]             integer  NOT NULL 
)
go

ALTER TABLE [Buyer]
	ADD CONSTRAINT [XPKBuyer] PRIMARY KEY  CLUSTERED ([BuyerId] ASC)
go

CREATE TABLE [City]
( 
	[CityId]             integer  IDENTITY ( 1,1 )  NOT NULL ,
	[Name]               varchar(100)  NULL 
)
go

ALTER TABLE [City]
	ADD CONSTRAINT [XPKCity] PRIMARY KEY  CLUSTERED ([CityId] ASC)
go

ALTER TABLE [City]
	ADD CONSTRAINT [XAKCityName] UNIQUE ([Name]  ASC)
go

CREATE TABLE [CityConnection]
( 
	[ConnectionId]       integer  IDENTITY ( 1,1 )  NOT NULL ,
	[Distance]           integer  NULL ,
	[City1Id]            integer  NOT NULL ,
	[City2Id]            integer  NOT NULL 
)
go

ALTER TABLE [CityConnection]
	ADD CONSTRAINT [XPKCityConnection] PRIMARY KEY  CLUSTERED ([ConnectionId] ASC)
go

CREATE TABLE [Order]
( 
	[OrderId]            integer  IDENTITY ( 1,1 )  NOT NULL ,
	[SentTime]           datetime  NULL ,
	[ReceivedTime]       datetime  NULL ,
	[State]              varchar(10)  NULL 
	CONSTRAINT [OrderStateValidation_272287454]
		CHECK  ( [State]='created' OR [State]='sent' OR [State]='arrived' ),
	[BuyerId]            integer  NOT NULL 
)
go

ALTER TABLE [Order]
	ADD CONSTRAINT [XPKOrder] PRIMARY KEY  CLUSTERED ([OrderId] ASC)
go

CREATE TABLE [OrderItem]
( 
	[ItemId]             integer  IDENTITY ( 1,1 )  NOT NULL ,
	[Count]              integer  NULL ,
	[OrderId]            integer  NOT NULL ,
	[ArticleId]          integer  NOT NULL ,
	[Discount]           integer  NULL 
)
go

ALTER TABLE [OrderItem]
	ADD CONSTRAINT [XPKOrderItem] PRIMARY KEY  CLUSTERED ([ItemId] ASC)
go

CREATE TABLE [Shop]
( 
	[ShopId]             integer  IDENTITY ( 1,1 )  NOT NULL ,
	[Name]               varchar(100)  NULL ,
	[Discount]           integer  NULL ,
	[CityId]             integer  NOT NULL 
)
go

ALTER TABLE [Shop]
	ADD CONSTRAINT [XPKShop] PRIMARY KEY  CLUSTERED ([ShopId] ASC)
go

ALTER TABLE [Shop]
	ADD CONSTRAINT [XAKShopName] UNIQUE ([Name]  ASC)
go

CREATE TABLE [Simulation]
( 
	[SimulationTime]     datetime  NULL ,
	[SimulationId]       integer  IDENTITY ( 1,1 )  NOT NULL 
)
go

ALTER TABLE [Simulation]
	ADD CONSTRAINT [XPKSimulation] PRIMARY KEY  CLUSTERED ([SimulationId] ASC)
go

CREATE TABLE [Transaction]
( 
	[TransactionId]      integer  IDENTITY ( 1,1 )  NOT NULL ,
	[Value]              decimal(10,3)  NULL ,
	[ExecutionTime]      datetime  NULL ,
	[OrderId]            integer  NOT NULL ,
	[ShopId]             integer  NULL ,
	[ExtraDiscount]      integer  NULL 
)
go

ALTER TABLE [Transaction]
	ADD CONSTRAINT [XPKTransaction] PRIMARY KEY  CLUSTERED ([TransactionId] ASC)
go


ALTER TABLE [Article]
	ADD CONSTRAINT [R_6] FOREIGN KEY ([ShopId]) REFERENCES [Shop]([ShopId])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go


ALTER TABLE [Buyer]
	ADD CONSTRAINT [R_2] FOREIGN KEY ([CityId]) REFERENCES [City]([CityId])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go


ALTER TABLE [CityConnection]
	ADD CONSTRAINT [R_3] FOREIGN KEY ([City1Id]) REFERENCES [City]([CityId])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [CityConnection]
	ADD CONSTRAINT [R_4] FOREIGN KEY ([City2Id]) REFERENCES [City]([CityId])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [Order]
	ADD CONSTRAINT [R_12] FOREIGN KEY ([BuyerId]) REFERENCES [Buyer]([BuyerId])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [OrderItem]
	ADD CONSTRAINT [R_8] FOREIGN KEY ([OrderId]) REFERENCES [Order]([OrderId])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go

ALTER TABLE [OrderItem]
	ADD CONSTRAINT [R_9] FOREIGN KEY ([ArticleId]) REFERENCES [Article]([ArticleId])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go


ALTER TABLE [Shop]
	ADD CONSTRAINT [R_5] FOREIGN KEY ([CityId]) REFERENCES [City]([CityId])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


ALTER TABLE [Transaction]
	ADD CONSTRAINT [R_10] FOREIGN KEY ([OrderId]) REFERENCES [Order]([OrderId])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go

ALTER TABLE [Transaction]
	ADD CONSTRAINT [R_11] FOREIGN KEY ([ShopId]) REFERENCES [Shop]([ShopId])
		ON DELETE NO ACTION
		ON UPDATE CASCADE
go

------------------ triggers ------------------

USE [OnlineProdaja]
GO

/****** Object:  Trigger [dbo].[TR_TRANSFER_MONEY_TO_SHOPS]    Script Date: 20-Jun-19 14:51:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[TR_TRANSFER_MONEY_TO_SHOPS]
ON [dbo].[Order]
AFTER UPDATE
AS
BEGIN
	
	DECLARE @OrderId INT
	DECLARE @PrevState VARCHAR(10)
	DECLARE @NewState VARCHAR(10)
	DECLARE @ReceivedTime DATETIME

	SELECT @OrderId = OrderId, @PrevState = [State]
	FROM deleted

	SELECT @NewState = [State], @ReceivedTime = ReceivedTime
	FROM inserted

	IF @PrevState = 'sent' AND @NewState = 'arrived'
	BEGIN
		
		DECLARE @ShopId INT
		DECLARE @ShopCursor CURSOR
		SET @ShopCursor = CURSOR FOR
		SELECT ShopId
		FROM Shop
		WHERE ShopId IN (
			SELECT sh.ShopId
			FROM Shop sh, Article ar, OrderItem oi
			WHERE sh.ShopId = ar.ShopId AND oi.ArticleId = ar.ArticleId AND oi.OrderId = @OrderId
		)

		OPEN @ShopCursor
		FETCH NEXT FROM @ShopCursor
		INTO @ShopId

		print('Package arrived!')

		WHILE @@FETCH_STATUS = 0
		BEGIN

			DECLARE @Amount DECIMAL(10,3)
			SELECT @Amount = 0.95 * SUM(ar.Price * oi.[Count] * (100 - oi.Discount) / 100)
			FROM [Order] [or], OrderItem oi, Article ar
			WHERE [or].OrderId = @OrderId AND oi.OrderId = [or].OrderId AND oi.ArticleId = ar.ArticleId AND ar.ShopId = @ShopId

			INSERT INTO [Transaction]([Value], ExecutionTime, OrderId, ShopId) VALUES (@Amount, @ReceivedTime, @OrderId, @ShopId)

			FETCH NEXT FROM @ShopCursor
			INTO @ShopId
		END

	END

END
GO

ALTER TABLE [dbo].[Order] ENABLE TRIGGER [TR_TRANSFER_MONEY_TO_SHOPS]
GO

--------------------- stored procedures --------------------


-------------------- SP_FINAL_PRICE --------------------

USE [OnlineProdaja]
GO

/****** Object:  StoredProcedure [dbo].[SP_FINAL_PRICE]    Script Date: 20-Jun-19 14:53:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SP_FINAL_PRICE]
	@OrderId INT,
	@FinalPrice DECIMAL(10,3) OUT
AS
BEGIN

	DECLARE @SpentLastMonth DECIMAL(10,3)

	SELECT @SpentLastMonth = SUM(Value)
	FROM [Transaction] tr, [Order] [or]
	WHERE	tr.[OrderId] = [or].OrderId AND
			[or].OrderId != @OrderId AND
			tr.ShopId IS NULL AND
			[or].BuyerId = (SELECT BuyerId FROM [Order] or1 WHERE or1.OrderId = @OrderId) AND
			DATEDIFF(DAY, tr.ExecutionTime, (SELECT SimulationTime FROM Simulation)) <= 30
	
	DECLARE @ItemCursor CURSOR
	DECLARE @ItemId INT

	SET @FinalPrice = 0.000
	SET @ItemCursor = CURSOR FOR
	SELECT ItemId
	FROM OrderItem
	WHERE OrderId = @OrderId

	OPEN @ItemCursor
	FETCH NEXT FROM @ItemCursor
	INTO @ItemId

	WHILE @@FETCH_STATUS = 0
	BEGIN

		DECLARE @ArticleCount INT
		DECLARE @ArticlePrice INT
		DECLARE @ItemDiscount INT

		SELECT @ArticleCount = oi.[Count], @ArticlePrice = ar.Price, @ItemDiscount = oi.Discount
		FROM OrderItem oi, Article ar
		WHERE oi.ItemId = @ItemId AND oi.ArticleId = ar.ArticleId

		SET @FinalPrice = @FinalPrice + @ArticleCount * @ArticlePrice * (100 - @ItemDiscount) / 100.000;

		FETCH NEXT FROM @ItemCursor
		INTO @ItemId
	END

	CLOSE @ItemCursor
	DEALLOCATE @ItemCursor

	IF @SpentLastMonth > 10000.000
	BEGIN
		SET @FinalPrice = 0.98 * @FinalPrice
	END

END
GO

-------------------- SPDiscountSum --------------------

USE [OnlineProdaja]
GO

/****** Object:  StoredProcedure [dbo].[SPDiscountSum]    Script Date: 20-Jun-19 14:54:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SPDiscountSum]
	@OrderId INT,
	@DiscountSum DECIMAL(10,3) OUT
AS
BEGIN
	
	DECLARE @PriceWithDiscount DECIMAL(10,3)
	EXEC [dbo].[SP_FINAL_PRICE] @OrderId, @PriceWithDiscount OUT

	DECLARE @ItemCursor CURSOR
	DECLARE @ItemId INT

	DECLARE @PriceWithoutDiscount DECIMAL(10,3)
	SET @PriceWithoutDiscount = 0.000
	SET @ItemCursor = CURSOR FOR
	SELECT ItemId
	FROM OrderItem
	WHERE OrderId = @OrderId

	OPEN @ItemCursor
	FETCH NEXT FROM @ItemCursor
	INTO @ItemId

	WHILE @@FETCH_STATUS = 0
	BEGIN

		DECLARE @ArticleCount INT
		DECLARE @ArticlePrice INT

		SELECT @ArticleCount = oi.[Count], @ArticlePrice = ar.Price
		FROM OrderItem oi, Article ar
		WHERE oi.ItemId = @ItemId AND oi.ArticleId = ar.ArticleId

		SET @PriceWithoutDiscount = @PriceWithoutDiscount + @ArticleCount * @ArticlePrice;

		FETCH NEXT FROM @ItemCursor
		INTO @ItemId
	END

	CLOSE @ItemCursor
	DEALLOCATE @ItemCursor

	SET @DiscountSum = @PriceWithoutDiscount - @PriceWithDiscount;

END
GO


-------------------- SPEraseAllData --------------------

USE [OnlineProdaja]
GO

/****** Object:  StoredProcedure [dbo].[SPEraseAllData]    Script Date: 20-Jun-19 14:54:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SPEraseAllData]
AS
BEGIN

	DBCC CHECKIDENT ([Article], RESEED, 0);
	DBCC CHECKIDENT ([Buyer], RESEED, 0);
	DBCC CHECKIDENT ([City], RESEED, 0);
	DBCC CHECKIDENT ([CityConnection], RESEED, 0);
	DBCC CHECKIDENT ([Order], RESEED, 0);
	DBCC CHECKIDENT ([OrderItem], RESEED, 0);
	DBCC CHECKIDENT ([Shop], RESEED, 0);
	DBCC CHECKIDENT ([Transaction], RESEED, 0);
	DBCC CHECKIDENT ([Simulation], RESEED, 0);

	EXEC sp_MSForEachTable 'DISABLE TRIGGER ALL ON ?'
	EXEC sp_MSForEachTable 'ALTER TABLE ? NOCHECK CONSTRAINT ALL'
	EXEC sp_MSForEachTable 'DELETE FROM ?'
	EXEC sp_MSForEachTable 'ALTER TABLE ? CHECK CONSTRAINT ALL'
	EXEC sp_MSForEachTable 'ENABLE TRIGGER ALL ON ?'

END
GO
