Just another online shopping API but with a built in delivery simulation.

The program keeps track of simulation's current time and has options for setting the simulation time or advancing it by some number of days.
There are cities connected by roads of different lengths and methods to calculate shortest paths between cities using Dijkstra's algorithm.
When a customer makes an order, shortest paths between different cities, where the shops and customer are located, are calculated.
By advancing the simulation time, order's location and/or status are updated, as well as transacations and every other time-dependent part of the system.
It takes x days to travel between two cities connected by a road of length x.
There are also some side features like discounts for regular customers etc.

Project was made for a univeristy course 'Softverski alati baza podataka' teaching various tools and ways of using databases.
